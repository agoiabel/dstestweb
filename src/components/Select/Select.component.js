import React from 'react';
import styles from './Select.component.module.css';

const Select = props => {
	let options;

	options = props.options.map(option => (
		<option value={option.value} key={option.value}>{option.displayValue}</option>
	));

	return (
		<div className={styles.formGroup}>
			<select className={styles.formControl} name={props.name} onChange={props.onChange}>
				{options}
			</select>
		</div>
	);
};

export default Select;