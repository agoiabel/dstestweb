import React from 'react';
import styles from './Header.module.css';

const Header = () => {
	return (
		<div className={styles.wrapper}>
			<div className={styles.container}>
				<div className={styles.logo}>DSTestWeb</div>				
				<div className={styles.bag}>BAG (1)</div>				
			</div>	
		</div>
	)
}

export default Header