import React from 'react';
import styles from './input.component.module.css';

const Input = props => {
	return (
	    <div className={styles.formGroup}>
	      <input className={styles.formControl} {...props} />
	    </div>
	);
}

export default Input;