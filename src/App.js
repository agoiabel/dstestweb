import React from 'react';
import Auth from './pages/Auth';
import Cart from './pages/Cart';
import SignUp from './pages/SignUp';
import Product from './pages/Product';
import Products from './pages/Products';
import CompleteOrder from './pages/CompleteOrder';
import {Route, Switch, Redirect} from 'react-router-dom';

class App extends React.Component {
  render() {
    return (
      
      <Switch>
        <Route path="/" exact component={SignUp} />
        <Route path="/auth" exact component={Auth} />
        <Route path="/products" exact component={Products} />
        <Route path="/product/:id" exact component={Product} />
        <Route path="/cart" exact component={Cart} />
        <Route path="/complete-order" exact component={CompleteOrder} />
        <Redirect to="/" />
      </Switch>

    );
  }
}

export default App;