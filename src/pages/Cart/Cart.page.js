import React from 'react';
import {connect} from 'react-redux';
import Button from '../../components/Button';
import Header from '../../components/Header';
import styles from './Cart.page.module.css';
import validate from '../../utils/validation';
import CustomInput from '../../components/Input';
import SelectInput from '../../components/Select';
import { shipping_address_for } from '../Product/Product.page.action';

class Cart extends React.Component {

	state = {
		showLoading: false,
		formIsValid: false,
		formControls: {
			address: {
				value: "",
				valid: false,
				validationRules: {
					isRequired: true
				},
				placeholderText: "Delivery Address",
				touched: false
			},
			state: {
				elementConfig: {
					options: [
						{
							value: 'Lagos',
							displayValue: 'Lagos',
						},
						{
							value: 'Abeokuta',
							displayValue: 'Abeokuta'
						},
						{
							value: 'Abuja',
							displayValue: 'Abuja'
						}
					]
				},
				value: '',
				validation: {
					required: true
				},
				valid: false,
				touched: false
			},
		}
	};


	navigateTo = page => {
		this.props.history.push(page);
	}


	inputChangeHandler = event => {

		const name = event.target.name;
		const value = (event.target.type === 'file') ? event.target.files[0] : event.target.value;

		const updatedControls = {
			...this.state.formControls
		};
		const updatedFormElement = {
			...updatedControls[name]
		};
		updatedFormElement.value = value;
		updatedFormElement.touched = true;
		updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

		updatedControls[name] = updatedFormElement;

		let formIsValid = true;
		for (let inputIdentifier in updatedControls) {
			formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
		}

		this.setState({
			formControls: updatedControls,
			formIsValid: formIsValid
		});
	}


	formSubmitHandler = event => {
		event.preventDefault();

		const formData = {};
		for (let formElementId in this.state.formControls) {
			formData[formElementId] = this.state.formControls[formElementId].value;
		}
		this.setState({
			showLoading: true
		});

		this.props.shipping_address_for(formData);
	};

	
	componentWillReceiveProps(nextProps) {
		if (nextProps.shipping_address !== 0) {
			return this.navigateTo('/complete-order');
		}
	}


	render () {


		let cart_items = this.props.cart_items.map(cart_item => (
			<div className={styles.bag} key={cart_item.product.id}>
				<div className={styles.bagImage}><img src={cart_item.product.image_path} alt="" /></div>
				<div className={styles.bagDescription}>
					<div className={styles.bagPrice}>NGN{cart_item.product.amount}</div>
					<div className={styles.bagTitle}>
						{cart_item.product.name}
					</div>
					<div className={styles.quantity}>
						<div className={styles.quantityName}>Quantity</div>
						<div className={styles.quantityNumber}>{cart_item.quantity}</div>
						<div className={styles.changeQuantity}>
							<i className="fa fa-caret-down"></i>
						</div>
					</div>
				</div>
				<div className={styles.close}><i className="fa fa-times" aria-hidden="true"></i></div>
			</div>
		));


		return (
			<div>
				<div>
					<Header />
				</div>


				<div className={styles.carts}>

					<div className={styles.myBags}>
						<div className={styles.bagHeader}>MY BAG</div>

						<div className={styles.bags}>
							{cart_items}							
						</div>
					</div>

					<div className={styles.shippingAddress}>

						<CustomInput type="text" name="address"
							placeholder={this.state.formControls.address.placeholderText}
							value={this.state.formControls.address.value}
							onChange={this.inputChangeHandler}
							valid={this.state.formControls.address.valid.toString()}
							touched={this.state.formControls.address.touched.toString()}
						/>

						<SelectInput
							name="state"
							value={this.state.formControls.state.value}
							placeholder={this.state.formControls.state.placeholder}
							options={this.state.formControls.state.elementConfig.options}
							onChange={this.inputChangeHandler}
							valid={this.state.formControls.address.valid.toString()}
						/>

						<Button onClick={this.formSubmitHandler} disabled={!this.state.formIsValid}>PROCEED SUMMARY</Button>
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		cart_items: state.productReducer.cart_items,
		shipping_address: state.productReducer.shipping_address
	}
}

const mapDispatchToProps = dispatch => {
	return {
		shipping_address_for: (formData) => dispatch(shipping_address_for(formData)),
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart);