import React from 'react';
import { connect } from 'react-redux';
import Button from '../../components/Button';
import Header from '../../components/Header';
import styles from './Products.page.module.css';



class Products extends React.Component {

	navigateTo = page => {
		// console.dir(page);
		this.props.history.push(page);
	}


	componentDidMount() {}


	render () {


		let products;

		if (typeof(this.props.products) !== undefined) {

			products = this.props.products.map(product => (
				<div className={styles.product} key={product.id}>
					<div className={styles.productImage}><img src={product.image_path} alt="" /></div>
					<div className={styles.productDescription}>
						<div className={styles.productTitle}><a onClick={() => this.navigateTo(`/product/${product.id}`)}>{product.name}</a></div>
						<div className={styles.productDetails}>
							<div>
								<Button>ADD TO CART</Button>
							</div>
							<div className={styles.productPrice}>NGN {product.amount}</div>
						</div>
					</div>
				</div>
			));

		}


		return (
			<div>
				<div>
					<Header />
				</div>
				

				<div className={styles.products}>
					{products}
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		products: state.authReducer.products
	}
}

const mapDispatchToProps = dispatch => {
	return {

	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Products);