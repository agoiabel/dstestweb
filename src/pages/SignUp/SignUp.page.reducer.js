import {updateObject} from '../../utils/updateObject';
import { SIGNUP_SUCCESSFUL, SIGNUP_UNSUCCESSFUL  } from "./index";

/**
 * Handle the process of updating the status for both success and failed
 * 
 * @param {} state 
 * @param {*} action 
 */
const reduceSignUp = (state, action) => {
    return updateObject(state, { 
        status: action.payload.status,
        message: action.payload.message 
    });
}


/**
 * Set the initial state
 */
const initialState = {
    status: null,
    message: null
};


const reducer = (state = initialState, action) => {

    const lookup = {
        SIGNUP_SUCCESSFUL: reduceSignUp,
        SIGNUP_UNSUCCESSFUL: reduceSignUp
    }

    return lookup[action.type] ? lookup[action.type](state, action) : state;
}


export default reducer;