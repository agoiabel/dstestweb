export {default} from './SignUp.page';


/** This Export the component actions */
export const START_SIGNUP = 'START_SIGNUP';
export const SIGNUP_SUCCESSFUL = 'SIGNUP_SUCCESSFUL';
export const SIGNUP_UNSUCCESSFUL = 'SIGNUP_UNSUCCESSFUL';