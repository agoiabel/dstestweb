import React from 'react';
import {connect} from 'react-redux';
import Button from '../../components/Button';
import Header from '../../components/Header';
import styles from './SignUp.page.module.css';
import CustomInput from '../../components/Input';
import validate from '../../utils/validation.js';
import { sign_up_start } from './SignUp.page.action';


class SignUp extends React.Component {

	state = {
		formIsValid: false,
		formControls: {
			firstname: {
				value: "",
				valid: false,
				validationRules: {
					isRequired: true
				},
				placeholderText: "firstname",
				touched: false
			},
			lastname: {
				value: "",
				valid: false,
				validationRules: {
					isRequired: true
				},
				placeholderText: "lastname",
				touched: false
			},
			phonenumber: {
				value: "",
				valid: false,
				touched: false,
				validationRules: {
					isRequired: true,
					minLength: 11,
					maxLength: 11
				},
				placeholderText: "Phone Number"
			},
			email: {
				value: "",
				valid: false,
				validationRules: {
					isEmail: true,
					isRequired: true
				},
				placeholderText: "Email Address",
				touched: false
			},
			password: {
				value: "",
				valid: false,
				touched: false,
				validationRules: {
					isRequired: true,
				},
				placeholderText: "Password"
			}
		}
	};

	navigateTo = page => {
		this.props.history.push(page);
	}

	inputChangeHandler = event => {

		const name = event.target.name;
		const value = (event.target.type === 'file') ? event.target.files[0] : event.target.value;

		const updatedControls = {
			...this.state.formControls
		};
		const updatedFormElement = {
			...updatedControls[name]
		};
		updatedFormElement.value = value;
		updatedFormElement.touched = true;
		updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

		updatedControls[name] = updatedFormElement;

		let formIsValid = true;
		for (let inputIdentifier in updatedControls) {
			formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
		}

		this.setState({
			formControls: updatedControls,
			formIsValid: formIsValid
		});
	}

	formSubmitHandler = event => {
		event.preventDefault();

		const formData = {};
		for (let formElementId in this.state.formControls) {
			formData[formElementId] = this.state.formControls[formElementId].value
		}

	    this.props.sign_up_start(formData);
	}


	componentWillReceiveProps(nextProps) {
		if (nextProps.status === 200) {
			//show success notification
			return this.navigateTo('/auth');
		}
		if ((nextProps.status === 422) || !(nextProps.message === this.props.message)) {
			//show notification
			return window.alert('Opps', nextProps.message);
		}
	}


	render () {
		return (
			<div>
				<Header />

				<div className={styles.signUpContainer}>
					<div className={styles.signUp}>SIGN UP</div>
					
					<div className={styles.signUpForm}>
						
						<CustomInput type="email" name="email" 
			                  placeholder={this.state.formControls.email.placeholderText}
			                  value={this.state.formControls.email.value}
			                  onChange={this.inputChangeHandler}
			                  valid={this.state.formControls.email.valid.toString()}
			                  touched={this.state.formControls.email.touched.toString()}
						/>

						<div className={styles.nameGroup}>
							<div className={styles.name}>
								<CustomInput type="text" name="firstname" 
					                  placeholder={this.state.formControls.firstname.placeholderText}
					                  value={this.state.formControls.firstname.value}
					                  onChange={this.inputChangeHandler}
					                  valid={this.state.formControls.firstname.valid.toString()}
					                  touched={this.state.formControls.firstname.touched.toString()}
								/>
							</div>
							<div className={styles.name}>
								<CustomInput type="text" name="lastname" 
					                  placeholder={this.state.formControls.lastname.placeholderText}
					                  value={this.state.formControls.lastname.value}
					                  onChange={this.inputChangeHandler}
					                  valid={this.state.formControls.lastname.valid.toString()}
					                  touched={this.state.formControls.lastname.touched.toString()}
								/>
							</div>
						</div>

						<CustomInput type="number" name="phonenumber" 
			                  placeholder={this.state.formControls.phonenumber.placeholderText}
			                  value={this.state.formControls.phonenumber.value}
			                  onChange={this.inputChangeHandler}
			                  valid={this.state.formControls.phonenumber.valid.toString()}
			                  touched={this.state.formControls.phonenumber.touched.toString()}
						/>

						<CustomInput type="password" name="password" 
			                  placeholder={this.state.formControls.password.placeholderText}
			                  value={this.state.formControls.password.value}
			                  onChange={this.inputChangeHandler}
			                  valid={this.state.formControls.password.valid.toString()}
			                  touched={this.state.formControls.password.touched.toString()}
						/>

						<Button onClick={this.formSubmitHandler} disabled={!this.state.formIsValid}>CREATE MY ACCOUNT</Button>

						<a className={styles.loginLink} onClick={() => this.navigateTo('/auth')}>Login</a>
						
					</div>
				</div>

			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		status: state.signUpReducer.status,
		message: state.signUpReducer.message
	}
}

const mapDispatchToProps = dispatch => {
	return {
	    sign_up_start: credentials => dispatch( sign_up_start(credentials) ),
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);