import { SIGNUP_SUCCESSFUL, SIGNUP_UNSUCCESSFUL } from "./index";
import { post } from '../../utils/http_client';

export const sign_up_start = payload => {
	return async dispatch => {
		try {

			const response = await post(JSON.stringify({
				email: payload.email,
				password: payload.password,
				firstname: payload.firstname,
				lastname: payload.lastname,
				phonenumber: payload.phonenumber
			}), 'user/store');

			const data = await response.json();

			if (data.status === 200) {
				return dispatch(sign_up_successful({
					status: data.status,
					message: data.message
				}));
			}
			return dispatch(sign_up_unsuccessful({
				status: data.status,
				message: data.message
			}));


		} catch (err) {
			return dispatch(sign_up_unsuccessful());
		}	
	}
}

export const sign_up_successful = payload => {
	return {
	    type: SIGNUP_SUCCESSFUL,
	    payload: payload
	}
}

export const sign_up_unsuccessful = payload => {
	return {
	    type: SIGNUP_UNSUCCESSFUL,
	    payload: payload
	}
}