import React from 'react';
import {connect} from 'react-redux';
import Button from '../../components/Button';
import Header from '../../components/Header';
import styles from './CompleteOrder.page.module.css';
import { place_order_for } from './CompleteOrder.page.action';

class CompleteOrder extends React.Component {


	placeOrderHandler = () => {
		let formData = {};
		formData['user_email'] = this.props.user.email;
		formData['cart_items'] = this.props.cart_items;
		formData['shipping_address'] = this.props.shipping_address;

		this.props.place_order_for(formData);
	}


	render() {
		return (
			<div>
				<div>
					<Header />
				</div>
				<div>
					<div className={styles.completeOrder}>

						<div className={styles.userInfo}>
							<div className={styles.basicInfo}>
								<div className={styles.title}>Basic Info</div>
								<div className={styles.content}>
									<div>{this.props.user.firstname} {this.props.user.lastname}</div>
									<div>{this.props.user.email}</div>
									<div>{this.props.user.phonenumber}</div>
								</div>
							</div>

							<div className={styles.shippingInfo}>
								<div className={styles.title}>Delivery Address</div>
								<div className={styles.content}>
									<div>{this.props.shipping_address.address} {this.props.shipping_address.state}</div>
									<div>{this.props.user.phonenumber}</div>
								</div>
							</div>							
							
							<div className={styles.placeOrder}>
								<Button onClick={this.placeOrderHandler}>Place the order</Button>
							</div>
						</div>

					</div>
				</div>
			</div>
		);		
	}
}


const mapStateToProps = state => {
	return {
		user: state.authReducer.user,
		cart_items: state.productReducer.cart_items,
		shipping_address: state.productReducer.shipping_address
	};
}

const mapDispatchToProps = dispatch => {
	return {
		place_order_for: formData => dispatch(place_order_for(formData)),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(CompleteOrder);