import { setStorage } from '../../utils/storage';
import { post } from '../../utils/http_client';
import { ORDER_PLACED_SUCCESSFUL } from './index';

export const place_order_for = payload => {
    return async dispatch => {

        try {
            let response = await post(JSON.stringify(payload), 'place_order/store');

            response = await response.json();

            const transaction = response.data.transaction;

            //move this to its own function if i will use in more than this place
            const handler = await window.PaystackPop.setup({
                key: 'pk_test_c6107f2bff6d8a2d211f6cce9b9067f612f29a14',
                email: transaction.user.email,
                amount: transaction.amount * 100,
                ref: transaction.reference,
                callback: function (response) {
                    console.dir(response);
                    dispatch(confirm_payment_for(response.reference));
                },
                onClose: function () {
                    console.dir('window closed');
                }
            });
            handler.openIframe();
        } catch (error) {
            console.dir(error);
        }

    }
};


export const product_sucessful = payload => {
    return {
        type: ORDER_PLACED_SUCCESSFUL,
        payload: payload
    };
}


export const confirm_payment_for = payload => {
    return async dispatch => {

        let response = await post(JSON.stringify({
            reference: payload
        }), 'place_order/confirm');

        response = await response.json();

        console.dir(response);

        // if (response.status === 200) {
        //     localStorage.removeItem('reduxStore');
        //     return dispatch(payment_successful(200));
        // }

        // return dispatch(payment_unsuccessful(422));
    }
}
