export {default} from './Product.page';

/** This Export the component actions */
export const PRODUCT_SUCCESSFUL = 'PRODUCT_SUCCESSFUL';
export const PRODUCT_UNSUCCESSFUL = 'PRODUCT_UNSUCCESSFUL';

export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';

export const SHIPPING_ADDRESS = 'SHIPPING_ADDRESS';