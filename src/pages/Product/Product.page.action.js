import { setStorage } from '../../utils/storage';
import { get } from '../../utils/http_client';
import { PRODUCT_SUCCESSFUL, PRODUCT_UNSUCCESSFUL, ADD_TO_CART, REMOVE_FROM_CART, SHIPPING_ADDRESS } from './index';


export const get_product_for = payload => {
    return async dispatch => {

        try {
            let response = await get(`product/show/${payload}`);

            response = await response.json();

            if (response.status === 422) {
                return window.setTimeout((() => {
                    dispatch(product_unsucessful(response));
                }));
            }

            //pass the token to redux
            dispatch(product_sucessful(response));
        } catch (error) {
            console.dir(error);
        }

    }
};

export const product_sucessful = payload => {
    return {
        type: PRODUCT_SUCCESSFUL,
        payload: payload
    };
}


export const product_unsucessful = payload => {
    return {
        type: PRODUCT_UNSUCCESSFUL,
        payload: payload
    }
}


export const add_to_cart = (product, quantity) => {
    return {
        type: ADD_TO_CART,
        payload: {
            quantity: quantity,
            product: product
        }
    }
}


export const remove_from_cart = array_key => {
    return {
        type: REMOVE_FROM_CART,
        payload: array_key
    }
}


export const shipping_address_for = payload => {
    return {
        type: SHIPPING_ADDRESS,
        payload: payload
    };
}

