import React from 'react';
import {connect} from 'react-redux';
import Modal from '../../components/Modal';
import Button from '../../components/Button';
import Header from '../../components/Header';
import styles from './Product.page.module.css';
import { get_product_for, add_to_cart, remove_from_cart } from './Product.page.action';

class Product extends React.Component {

	state = {
		product_quantity: 1,
		added_to_cart: 0 ,
		showModal: false
	}

	navigateTo = page => {
		this.props.history.push(page);
	}

	componentDidMount() {
		this.props.get_product_for(this.props.match.params.id);
	}

	componentDidUpdate(prevProps) {
		if (prevProps.match.params.id !== this.props.match.params.id) {
			this.props.get_product_for(this.props.match.params.id);
		}
	}

	addToCartHandler = (product, quantity, cart_items) => {
		if (cart_items.length) {
			cart_items.map((cart_item, index) => {
				if (cart_item.product.id === product.id) {
					this.props.remove_from_cart(index);
				}
			});
		}

		this.props.add_to_cart(product, quantity);

		this.setState(prevState => ({
			added_to_cart: 1,
			showModal: !prevState.showModal
		}));
	}


	render () {

		let modal;
		let productContainer = 'loading';
		const {product} = this.props;

		if (product !== 0 ) {
			productContainer = (
				<div className={styles.singleProduct}>
					<div className={styles.singleProductImage}><img src={product.image_path} alt="" /></div>
					<div className={styles.singleProductDetails}>
						<div className={styles.singleProductTitle}>{product.name}</div>
							<div className={styles.singleProductDescription}>{product.description}</div>
							<div className={styles.priceQuantity}>
								<div className={styles.price}>
								<div className={styles.priceHeader}>PRICE</div>
								<div className={styles.amount}>NGN {product.amount}</div>
								</div>
								<div className={styles.quantity}>
								<div className={styles.quantityHeader}>QUANTITY</div>
								<div className={styles.quantityCounter}>
									<div className={styles.quantityDisplay}>1</div>
									<div className={styles.counter}>
										<div className={styles.addition}> + </div>
										<div className={styles.subtraction}> - </div>
										</div>
									</div>
								</div>
							</div>
							<div className={styles.addToCart}>
								<Button onClick={() => this.addToCartHandler(product, this.state.product_quantity, this.props.cart_items)}>
									ADD TO CART
								</Button>
							</div>
						</div>
				</div>
			);
		}

		if (this.state.showModal) {
			modal = (
				<div className="modal">
					<div className="modal-content">
						<div className="modal-header">
							<img src={require('../../assets/images/success.png')} className="" />
							<div className="modal-title">item added to cart successfully</div>
						</div>

						<div>
							<div className="modal-button">
								<Button onClick={() => this.navigateTo('/cart')}>VIEW CART</Button>
							</div>
							<div className="modal-button">
								<Button onClick={() => this.navigateTo('/products')}>CONTINUE SHOPPING</Button>
							</div>
						</div>
					</div>
				</div>
			)
		}


		return (
			<div>
				{modal}
				<div>
					<Header />
				</div>
				{productContainer}
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		product: state.productReducer.product,
		cart_items: state.productReducer.cart_items
	}
}

const mapDispatchToProps = dispatch => {
	return {
		get_product_for: productId => dispatch(get_product_for(productId)),
		add_to_cart: (product, quantity) => dispatch( add_to_cart(product, quantity) ),
		remove_from_cart: (array_key) => dispatch( remove_from_cart(array_key) )
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Product);