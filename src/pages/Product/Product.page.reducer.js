import { updateObject } from '../../utils/updateObject';
import { PRODUCT_SUCCESSFUL, PRODUCT_UNSUCCESSFUL, ADD_TO_CART, REMOVE_FROM_CART, SHIPPING_ADDRESS} from './index';


const productWasSucessful = (state, action) => {
    return updateObject(state, {
        product: action.payload.data.product,
        status: action.payload.status
    });
}


const productWasNotSucessful = (state, action) => {
    return updateObject(state, {
        message: action.payload.message,
        status: action.payload.status
    });
}


const addToCart = (state, action) => {
    return updateObject(state, {
        cart_items: state.cart_items.concat(action.payload),
        sub_price: state.sub_price + (parseInt(action.payload.product.amount) * parseInt(action.payload.quantity))
    });
}


const shippingAddress = (state, action) => {
    return updateObject(state, {
        shipping_address: action.payload
    });
}


const initialState = {
    status: null,
    user: null,
    message: null,
    product: 0,

    cart_items: [],
    sub_price: 0,
    shipping_address: 0
};

const productReducer = (state = initialState, action) => {

    const lookup = {
        PRODUCT_SUCCESSFUL: productWasSucessful,
        PRODUCT_UNSUCCESSFUL: productWasNotSucessful,
        ADD_TO_CART: addToCart,
        SHIPPING_ADDRESS: shippingAddress,
    }

    return lookup[action.type] ? lookup[action.type](state, action) : state;

}

export default productReducer;