import React from 'react';
import {connect} from 'react-redux'; 
import styles from './Auth.page.module.css';
import Button from '../../components/Button';
import Header from '../../components/Header';
import validate from '../../utils/validation';
import CustomInput from '../../components/Input';
import { start_login } from './Auth.page.action';

class Auth extends React.Component {

	state = {
		showLoading: false,
		formIsValid: false,
		formControls: {
			email: {
				value: "",
				valid: false,
				validationRules: {
					isEmail: true,
					isRequired: true
				},
				placeholderText: "Email Address",
				touched: false
			},
			password: {
				value: "",
				valid: false,
				touched: false,
				validationRules: {
					isRequired: true
				},
				placeholderText: "Password"
			}
		}
	};


	navigateTo = page => {
		this.props.history.push(page);
	}



	inputChangeHandler = event => {

		const name = event.target.name;
		const value = (event.target.type === 'file') ? event.target.files[0] : event.target.value;

		const updatedControls = {
			...this.state.formControls
		};
		const updatedFormElement = {
			...updatedControls[name]
		};
		updatedFormElement.value = value;
		updatedFormElement.touched = true;
		updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

		updatedControls[name] = updatedFormElement;

		let formIsValid = true;
		for (let inputIdentifier in updatedControls) {
			formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
		}

		this.setState({
			formControls: updatedControls,
			formIsValid: formIsValid
		});
	}



	formSubmitHandler = () => {
		const formData = {};
		for (let formElementId in this.state.formControls) {
			formData[formElementId] = this.state.formControls[formElementId].value;
		}
		this.setState({
			showLoading: true
		});

		this.props.start_login(formData);
	};

	
	componentWillReceiveProps(nextProps) {
		if (nextProps.status === 200) {
			this.setState({
				showLoading: false
			});
			this.navigateTo('/products');
		}
		if ((nextProps.status === 422) && !(nextProps.message === this.props.message)) {
			//show notification
			this.setState({
				showLoading: false
			});
			return window.alert('Opps', nextProps.message);
		}
	}


	render() {
		return (
			<div>
				<Header />

				<div className={styles.container}>
					<div className={styles.signIn}>SIGN IN</div>

					<div className={styles.signInForm}>
						<CustomInput type="email" name="email"
							placeholder={this.state.formControls.email.placeholderText}
							value={this.state.formControls.email.value}
							onChange={this.inputChangeHandler}
							valid={this.state.formControls.email.valid.toString()}
							touched={this.state.formControls.email.touched.toString()}
						/>

						<CustomInput type="password" name="password"
							placeholder={this.state.formControls.password.placeholderText}
							value={this.state.formControls.password.value}
							onChange={this.inputChangeHandler}
							valid={this.state.formControls.password.valid.toString()}
							touched={this.state.formControls.password.touched.toString()}
						/>

						<Button onClick={this.formSubmitHandler} disabled={!this.state.formIsValid}>LOGIN</Button>
					</div>
				</div>

			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		status: state.authReducer.status,
		message: state.authReducer.message
	}
}

const mapDispatchToProps = dispatch => {
	return {
		start_login: credentials => dispatch(start_login(credentials)),
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Auth);