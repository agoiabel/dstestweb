import {combineReducers} from 'redux';

import cartReducer from '../pages/Cart/Cart.page.reducer';
import authReducer from '../pages/Auth/Auth.page.reducer';
import signUpReducer from '../pages/SignUp/SignUp.page.reducer';
import productReducer from '../pages/Product/Product.page.reducer';

export default combineReducers({
    authReducer: authReducer,
    cartReducer: cartReducer,
    signUpReducer: signUpReducer,
    productReducer: productReducer,
});